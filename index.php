<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--<link rel="stylesheet/less" type="text/css" href="assets/less/style.less" />-->
    <script src="assets/js/less.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
    <div style="height: 100%; width: 100%; background-color: #FFDDFF">
        <div class="login" style="background-color: #FDD">
            <form class="form-login">
                <label>User Name</label><br>
                <div class="tooltip">
                    <input id="name" class="inputlogin" type="text" />
                    <span class="tooltiptext">Verifica tu usuario</span>
                </div>
                <label>Password</label><br>
                <div class="tooltip">
                    <input id="pass" class="inputlogin" type="password" />
                    <span class="tooltiptext">Verifica tu contraseña</span>
                </div>
                <button type="button" onClick="sendData()">Ingresar</button>
            </form>
        </div>
    </div>
</body>

<script>
    function sendData() {
        var user = document.getElementById('name').value;
        var pass = document.getElementById('pass').value;

        var data = "username=" + user + "&password=" + pass+"&seccion=login";
        var url = "assets/php/functions.php";

        var header = {
            method: 'POST',
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            mode: 'no-cors',
            body: data
        }

        if (user != "" && pass != "") {
            fetch(url, header)
            .then(response => response.text())
            .then(function(response){
                var resp = JSON.parse(response);
                var data2 = "user=" + resp["idEmpleado"] + "&seccion=modulos";
                fetch(url, {
                    method: 'POST',
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    mode: 'no-cors',
                    body: data2
                })
                .then(response => response.text())
                .then(function(response){
                    if (response != 'Error') {
                        window.location = "modulos/dashboard/dashboard.php"
                    }
                })
                .catch(function(error){
                    console.log("ERROR");
                });
            })
            .catch(function(error){
                var divError = document.getElementsByClassName("tooltip"); 
                var spanError = document.getElementsByClassName("tooltiptext"); 
                for (i = 0; i < divError.length; i++) {  
                    divError[i].style.visibility = "visible";
                    spanError[i].style.visibility = "visible";
                }
                document.getElementById("name").style.borderColor = "red"; 
                document.getElementById("pass").style.borderColor = "red"; 
                console.log("Error");
            });
        } else {
            document.getElementById("name").style.borderColor = "red"; 
            document.getElementById("pass").style.borderColor = "red"; 
        }
    }
</script>
</html>