function getEmpl(param1, param2, param3) {
    var url = "../../assets/php/functions.php";
    var data = "seccion=lista";
    var header = {
        method: 'POST',
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        mode: 'no-cors',
        body: data
    }
    fetch(url, header)
    .then(response => response.text())
    .then(function(response){
        window.location = "../"+param1.toLowerCase()+"/"+param2;
    });
}