function redirect(param1, param2, param3){
    switch (param1) {
        case 'Empleado':
            //window.location = "../"+param1.toLowerCase()+"/"+param2;
            if (param3 == 'Listado') {
                getEmpl(param1, param2, param3);
            } else if (param3 == 'Alta Candidatos' 
                    || param3 == 'Alta Candidatos' 
                    || param3 == 'Citar' 
                    || param3 == 'Entrevistar' 
                    || param3 == 'Examen'  
                    || param3 == 'Documentos' 
                    || param3 == 'Contratar') {
                window.location = "../"+param1.toLowerCase()+"/"+param2;
            }
            break;
        case 'Nomina':
            window.location = "../"+param1.toLowerCase()+"/"+param2;
            break;
        case 'Asistencia':
            window.location = "../"+param1.toLowerCase()+"/"+param2;
            break;
        case 'Dashboard':
            window.location = "../"+param1.toLowerCase()+"/"+param2;
            break;
        default:
            window.location = "../"+param1.toLowerCase()+"/"+param2;
            break;
    }
}
/*****************************************
 * BLOQUE FUNCIONES ARCHIVO VER EMPLEADOS
*****************************************/
function activate(param1, param2) {
    ActionModal(param1, param2,'abrir','activate');
}
function desactivate(param1, param2) {
    ActionModal(param1, param2,'abrir','desactivate');
}
function denegar() {
    ActionModal('cerrar');
}
function editInfo(param1, param2, param3, param4) {
    switch (param2) {
        case 'Modificar':
            window.location = "../"+param3.toLowerCase()+"/"+param4+"?user="+param1;
            break;
    }
}
function ActionModal(param1,param2,param3, param4) {
    var modal = document.getElementById('myModal');
    var titleM = document.getElementById('titleM');
    var bodyM = document.getElementById('bodyM');
    var MObjct = document.getElementById('MObjct');
    var span = document.getElementsByClassName("close")[0];

    if (param3 == 'abrir') {
        document.getElementById("modal-content").style.width = "30%"; 

        modal.style.display = "block";
        if (param4 == 'activate') {
            titleM.innerHTML  = "Activar usuario"
            bodyM.innerHTML  = "¿Desea activar este usuario?"
            MObjct.innerHTML  = param1 + " " + param2
        } else if (param4 == 'desactivate') {
            titleM.innerHTML  = "Desactivar usuario"
            bodyM.innerHTML  = "¿Desea dar de baja este usuario?"
            MObjct.innerHTML  = param1 + " " + param2
        }
    } else {
        modal.style.display = "none";
    }

    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}
/*********************************************
* FIN BLOQUE FUNCIONES ARCHIVO VER EMPLEADOS
*********************************************/
/*var bucle = setInterval(function(){
    salir();
},60000);*/
function salir(){
    var url = "../../assets/php/functions.php";
    var data = "seccion=delete";
    fetch(url, {
            method: 'POST',
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            mode: 'no-cors',
            body: data
    })
    .then(response => response.text())
    .then(function(response){
        window.location = "../../index.php"
    })
    .catch(function(error){
        console.log("ERROR");
    });
}