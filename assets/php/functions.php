<?php
    require('../../assets/php/dbconnection.php');
    require('array_group_by.php');

    if ($_POST['seccion'] === 'modulos' && isset($_POST['user'])) {
        
        $user = $_POST['user'];
        $sql = "CALL get_modulos($user)";
        $arrayCard = array();

        $result = mysqli_query(OpenCon(), $sql);
        if (mysqli_num_rows($result) >= 0) {
            
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                array_push($arrayCard, $row);
            }

            $grouped = array_group_by($arrayCard, 'title');

            $nombre_fichero = '../json/';

            if (!file_exists($nombre_fichero)) {
                mkdir($nombre_fichero, 0777, true);
            }
            $my_file = '../json/menu.json';
            $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
            $data = json_encode($grouped);
            fwrite($handle, $data);
            fclose($my_file);
        }

    } else if ($_POST['seccion'] === 'login' && isset($_POST['username']) && isset($_POST['password'])) {

        $username = $_POST['username'];
        #$password = base64_encode($_POST['password']);
        $password = $_POST['password'];
        
        $sqlL = "CALL get_user_login('$username', '$password')";

        $resultL = mysqli_query(OpenCon(), $sqlL);
        if (mysqli_num_rows($resultL) > 0) {
            session_start();
            while ($rowL = mysqli_fetch_array($resultL, MYSQLI_ASSOC)) {
                $_SESSION["idEmpleado"] = $rowL['idEmpleado'];
                $_SESSION["nombreEmpleado"] = $rowL['empleado'];
                $_SESSION["departamento"] = $rowL['departamento'];
                echo(json_encode($rowL));
            }
        } else {
            echo('Error');
        }

    } else if ($_POST['seccion'] === 'delete') {

        unlink('../json/menu.json');
        unlink('../json/empleados.json');
        session_start();
        
        $_SESSION["idEmpleado"] = 0;
        $_SESSION["nombreEmpleado"] = 0;
        $_SESSION["departamento"] = 0;
        session_destroy();

        echo("Success");

    } else if ($_POST['seccion'] === 'lista') {

        $sqlE = "CALL get_empleados()";
        $arrayEmpl = array();

        $resultE = mysqli_query(OpenCon(), $sqlE);
        if (mysqli_num_rows($resultE) >= 0) {
            
            $arrayEmpl = $resultE->fetch_all(MYSQLI_ASSOC);
            /*while ($rowE = mysqli_fetch_array($resultE, MYSQLI_ASSOC)) {
                array_push($arrayEmpl, $rowE);
            }*/
            $nombre_fichero = '../json/';

            if (!file_exists($nombre_fichero)) {
                mkdir($nombre_fichero, 0777, true);
            }
            $my_file = '../json/empleados.json';
            $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
            $data = json_encode($arrayEmpl);
            fwrite($handle, $data);
            fclose($my_file);
        }

    } else if ($_POST['seccion'] === 'updateUser' && isset($_POST['data'])) {

        $receivedDara = $_POST['data'];
        if (strpos($receivedDara, 'BAJA') !== false) {
            $split = explode('-', $receivedDara);
            $split2 = explode(' ', $split[0]);
            $idRev = trim($split2[0]);
            $user = trim($split[1]);
        } else {
            $split = explode(' ', $receivedDara);
            $idRev = trim($split[0]);
            $user = trim($split[1]);
        }
        
        $sqlRevi = "CALL revivir_empleado($idRev, '$user')";
    
        if (OpenCon()->query($sqlRevi) === TRUE) {
            echo "Success";
        } else {
            echo "Error updating record: " .OpenCon()->error;
        }

    } else if ($_POST['seccion'] === 'killUser' && isset($_POST['data'])) {

        $recData = $_POST['data'];
        $split = explode(' ', $recData);
        $sqlBaja = "CALL baja_empleado('$split[0]')";
        if (OpenCon()->query($sqlBaja) === TRUE) {
            echo "Success";
        } else {
            echo "Error updating record: " .OpenCon()->error;
        }

    } else {
        echo("Empty");
    }
?>