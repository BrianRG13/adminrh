<?php 
    function OpenCon() {
        $dbhost = "localhost";
        $dbuser = "root";
        $dbpass = "";
        $db = "prueba";
        $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

        /* cambiar el conjunto de caracteres a utf8 */
        if (!$conn->set_charset("utf8")) {
            exit();
        } else {
        }

        return $conn;
    }
    function CloseCon() {
        OpenCon() -> close();
    }
?>