<?php
    if (!session_start()) {
        session_start();
    }
    if (!isset($_SESSION["idEmpleado"])) {
        header("Location: ../../");
        session_destroy();
    }
?>