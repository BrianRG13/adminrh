<?php
    include('../../assets/php/cargarmenu.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Dashboard</title>
        <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet"> 
        <!--<link rel="stylesheet/less" type="text/css" href="../../assets/less/style.less" />-->
        <script src="../../assets/js/less.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
        <script defer src="../../assets/js/fontawesome.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
    </head>
    <body>
        <?php include('../../extensions/topnavbar.php'); ?>
        <?php include('../../extensions/leftnavbar.php'); ?>
        <div class="container-mod-empleado">
            <section class="content-header">
                <h1>
                    Empleado
                    <small>Modificar Empleado</small>
                </h1>
            </section>
            <div class="contenedor-mod form-style-6 card-mod-1">
                <form>
                    <legend>Información del Empleado</legend>
                    <fieldset class="fieldset-mod-user">
                        <input type="text" name="field1" placeholder="Nombre(s)" disabled>
                        <input type="text" name="field2" placeholder="Apellido Paterno" disabled>
                        <input type="text" name="field3" placeholder="Apellido Materno" disabled>
                        <input type="text" name="field4" placeholder="Teléfono *">
                        <input type="text" name="field1" placeholder="Domicilio">
                        <input type="text" name="field2" placeholder="Código Postal">
                        <input type="text" name="field3" placeholder="No. Ext.">
                    </fieldset>
                    <input type="submit" value="Guardar" />
                    <input type="reset" value="Limpiar" />
                </form>
            </div>
        </div>
    </body>
    <script src="../../assets/js/functions.js"></script>
    <script src="../../assets/js/treatment.js"></script>
</html>