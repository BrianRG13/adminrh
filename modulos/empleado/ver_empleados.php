<?php
    include('../../assets/php/cargarmenu.php');
    $infoE = file_get_contents('../../assets/json/empleados.json');
    $ArrayLE = json_decode($infoE, true);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Dashboard</title>
        <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="../../assets/css/jquery.dataTables.min.css">
        <!--<link rel="stylesheet/less" type="text/css" href="../../assets/less/style.less" />-->
        <script src="../../assets/js/less.min.js" ></script>
        <script src="../../assets/js/jquery-3.3.1.js" ></script>
        <script src="../../assets/js/jquery.dataTables.min.js" ></script>
        <script defer src="../../assets/js/fontawesome.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
        <script src="../../assets/js/treatment.js"></script>
        <script src="../../assets/js/functions.js"></script>
        <script>
        </script>
    </head>
    <body>
        <?php include('../../extensions/topnavbar.php'); ?>
        <?php include('../../extensions/leftnavbar.php'); ?>
        <div id="divTble" class="container">
            <table align="center" class="table">
                <thead class="thead-dark">
                    <tr width="100%">
                        <th width="5%">#</th>
                        <th width="25%">Nombre</th>
                        <th width="20%">Fecha Alta</th>
                        <th width="20%">Fecha Baja</th>
                        <th width="10%">Estatus</th>
                        <th width="15%">Detalles</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ArrayLE as $key) { ?>
                    <tr width="100%" align="center" class="body-table">
                        <td width="5%"><?php echo($key['id']); ?></td>
                        <td width="25%"><?php echo($key['nombre']); ?></td>
                        <td width="20%"><?php echo($key['alta']); ?></td>
                        <td width="20%"><?php echo($key['baja']); ?></td>
                        <td width="10%">
                            <?php if ($key['baja'] != '-') { ?>
                            <i class="fas fa-ban" title="Desactivado"></i>
                            <?php } else { ?>
                            <i class="fas fa-check-circle" title="Activado"></i>
                            <?php } ?>
                        </td>
                        <td width="15%">
                            <?php if ($key['baja'] != '-') { ?>
                            <i id="action" class="fas fa-plus-circle" title="Activar" onClick="activate('<?php echo($key['id']); ?>', '<?php echo($key['nombre']); ?>')"></i>
                            <?php } else { ?>
                            <i id="action" class="fas fa-minus-circle" title="Baja" onClick="desactivate('<?php echo($key['id']); ?>', '<?php echo($key['nombre']); ?>')"></i>
                            <i class="fas fa-user-edit" title="Editar" onClick="editInfo('<?php echo($key['id']); ?>','Modificar', 'empleado', 'mod_user.php')"></i>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php include('../../extensions/modal.php') ?>
    </body>
    <script type="text/javascript">
        function aceptar(param) {
            var MObjct = document.getElementById('MObjct').innerHTML;
            var titleM = document.getElementById('titleM').innerHTML;
            var url = "../../assets/php/functions.php";
            var seccion
            if (titleM == 'Activar usuario') {
                seccion = 'updateUser'
            } else if (titleM == 'Desactivar usuario') {
                seccion = 'killUser'
            }
            var data = "seccion="+seccion + "&data="+MObjct + "&idEmpleado="+param;
            var header = {
                method: 'POST',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                mode: 'no-cors',
                body: data
            }
            fetch(url, header)
            .then(response => response.text())
            .then(function(response){
                if (response == 'Success') {
                    redirect('Empleado', '<?php echo(basename(__FILE__)); ?>', 'Listado');
                    ActionModal('cerrar');
                }
            });
        }
        $(document).ready(function() {
            $('.table').DataTable( {
                "pagingType": "simple_numbers",
                scrollY:        '65vh',
                scrollCollapse: true,
                paging:         false,
                "dom":' <"search"fl><"top">rt<"bottom"ip><"clear">',
                "ordering": false,
                "filter": true,
                "info" : true,
                "language": {
                    "info": "Mostrando un total de _TOTAL_ resultados",
                    "search": "Buscar: ",
                    "infoEmpty": "Información no disponible",
                    "zeroRecords": "No se encontró información",
                    "searchPlaceholder": "Buscar información",
                    "sInfoFiltered": " - filtrados de _MAX_ resultados",
                    "sLoadingRecords": "Cargando - Por favor espere..."
                }
            });
        });
    </script>
</html>