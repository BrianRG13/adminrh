<?php
    require('../../assets/php/cargarmenu.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Dashboard</title>
        <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet"> 
        <!--<link rel="stylesheet/less" type="text/css" href="../../assets/less/style.less" />-->
        <script src="../../assets/js/less.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
        <script defer src="../../assets/js/fontawesome.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
    </head>
    <body>
        <ul>
            <li class="topMenu" style="float:right"><a onClick="salir();">Salir</a></li>
            <li class="welcome" style="float:left"><a>Bienvenido, <b class="person"><?php echo "$_SESSION[nombreEmpleado]" ?></b> / Departamento, <b class="person"><?php echo($_SESSION["departamento"]); ?></b></a></li>
        </ul>
        <?php require('../../extensions/leftnavbar.php'); ?>
        <div class="container-alta-empleado">
            <section class="content-header">
                <h1>
                    Empleado
                    <small>Alta Candidato</small>
                </h1>
            </section>
            <div class="contenedor-alta form-style-5 card-alta">
                <form>
                    <fieldset>
                        <legend>Información del Candidato</legend>
                        <input type="text" name="field1" placeholder="Nombre(s) *">
                        <input type="text" name="field2" placeholder="Apellido Paterno *">
                        <input type="text" name="field3" placeholder="Apellido Materno *">
                        <input type="text" name="field4" placeholder="Celular *">
                        <label for="job">Origen del Candidato</label>
                        <select id="job" name="field4">
                            <optgroup label="Bolsas de Trabajo">
                            <option value="occ">OCC</option>
                            <option value="facebook">Facebook</option>
                            <option value="indeed">Indeed</option>
                            <option value="computrabajo">Computrabajo</option>
                            </optgroup>
                        </select>      
                    </fieldset>
                    <input type="submit" value="Guardar" />
                    <input type="reset" value="Limpiar" />
                </form>
            </div>
        </div>
    </body>
    <script src="../../assets/js/functions.js"></script>
    <script src="../../assets/js/treatment.js"></script>
</html>