<?php
    require('../../assets/php/cargarmenu.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Dashboard</title>
        <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet"> 
        <!--<link rel="stylesheet/less" type="text/css" href="../../assets/less/style.less" />-->
        <script src="../../assets/js/less.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
        <script defer src="../../assets/js/fontawesome.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
    </head>
    <body>
        <?php include('../../extensions/topnavbar.php'); ?>
        <div class="row">
            <?php foreach ($json_data as $key => $value) { ?>
            <div class="column">
                <div class="card">
                    <h3 class="titleCard"><?php echo($key); ?><hr></h3>
                    <?php foreach ($value as $index => $element) { ?>
                        <button class="optionCard" onClick="redirect('<?php echo($key); ?>','<?php echo($element["dir"]); ?>', '<?php echo($element["subtitle"]); ?>')"><i class="<?php echo($element["icon"]); ?>"></i> <?php echo($element["subtitle"]); ?></button>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </body>
    <script src="../../assets/js/functions.js"></script>
    <script src="../../assets/js/treatment.js"></script>
</html>