<div id="myModal" class="modal">
    <div id="modal-content" class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h2 id="titleM"></h2>
        </div>
        <div class="modal-body">
            <p id="bodyM">Some text in the Modal Body</p>
            <p id="MObjct">Some text in the Modal Body</p>
        </div>
        <div class="modal-footer">
            <button class="buttonM" type="button" onClick="aceptar('<?php echo($_SESSION["idEmpleado"]);?>')">SI</button>
            <button class="buttonM" type="button" onClick="denegar()">NO</button>
        </div>
    </div>
</div>