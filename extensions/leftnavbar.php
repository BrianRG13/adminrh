<div class="side-bar">
    <nav>
        <ul>
            <li>
                <a onClick="redirect('Dashboard','dashboard.php')">
                    <i class="fas fa-home menu_icon"></i>
                    INICIO
                </a>
            </li>
            <?php foreach ($json_data as $key => $value) { ?>
                <?php foreach ($value as $index => $element) { ?>
                <li>
                    <a class="<?php if (basename($_SERVER['PHP_SELF']) == $element['dir']) { ?> active <?php } ?>" onClick="redirect('<?php echo($key); ?>','<?php echo($element["dir"]); ?>', '<?php echo($element["subtitle"]); ?>')"> 
                        <i class="<?php echo($element['icon']); ?> menu_icon "></i>
                        <?php echo($element['subtitle']); ?>
                    </a>
                </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </nav>
</div>